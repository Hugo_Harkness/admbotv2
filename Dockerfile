FROM node:alpine
WORKDIR /app

ENV TZ="Europe/Paris"
#Deps
RUN apk add --no-cache tzdata ffmpeg

#Build Deps
RUN apk add --no-cache --virtual .build-deps make libtool autoconf automake g++ python3

COPY bot/package.json .
COPY bot/package-lock.json .
RUN npm install
RUN apk del .build-deps

COPY bot/tsconfig.json .
COPY bot/src/ ./src/
RUN npx tsc

CMD node bin/index.js
