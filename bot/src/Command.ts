import {
    ApplicationCommandChoicesData,
    ApplicationCommandData,
    ApplicationCommandNonOptionsData,
    ApplicationCommandOptionData,
    ApplicationCommandPermissionData,
    ApplicationCommandSubCommandData,
    CommandInteraction, ContextMenuInteraction,
    Guild
} from "discord.js";

export class CommandGrp {

    readonly subCommandGrps: Array<SubCommandGrp>;
    readonly subCommands: Array<SubCommand>;

    readonly userPermissions: Array<ApplicationCommandPermissionData>;
    readonly roleNamePermissions: Array<{ name: String, permission: boolean }>;

    readonly name: string;
    readonly description: string;
    readonly type: "CHAT_INPUT" | "USER" | "MESSAGE";

    constructor(name: string, description: string,
                subCommandGrps: Array<SubCommandGrp> = [],
                subCommands: Array<SubCommand> = [],
                roleNamePermissions: Array<{ name: String, permission: boolean }> = [],
                userPermissions: Array<ApplicationCommandPermissionData> = [],
                type: "CHAT_INPUT" | "USER" | "MESSAGE" = "CHAT_INPUT") {
        this.name = name;
        this.description = description;
        this.subCommandGrps = subCommandGrps;
        this.subCommands = subCommands;
        this.roleNamePermissions = roleNamePermissions;
        this.userPermissions = userPermissions;
        this.type = type;
    }

    public run(commandInteraction: CommandInteraction){
        const subCommandGrpName = commandInteraction.options.getSubcommandGroup(false);
        const subCommandName = commandInteraction.options.getSubcommand(true);
        if (!subCommandName) return;

        if (!subCommandGrpName) {
            this.subCommands
                .find((subCommand) => (subCommand.getCommandDefinition().name == subCommandName))
                ?.run(commandInteraction);
        } else {
            this.subCommandGrps
                .find((subCommandGrp) => (subCommandGrp.getCommandDefinition().name == subCommandGrpName))
                ?.subCommands
                .find((subCommand) => (subCommand.getCommandDefinition().name == subCommandName))
                ?.run(commandInteraction);
        }
    }

    public getCommandDefinition(): ApplicationCommandData {
        return {
            "name": this.name,
            "description": this.description,
            "type": this.type,
            "options": this.subCommandGrps.map((subCommandGrp) => {return subCommandGrp.getCommandDefinition();})
                .concat(this.subCommands.map((subCommand) => {return subCommand.getCommandDefinition();}))
        };
    };

    public getPermissions(guild: Guild) {
        let rolePermissions: Array<ApplicationCommandPermissionData>
            = this.roleNamePermissions.map(roleNamePermission => {
            let role = guild.roles.cache.find(role => (role.name === roleNamePermission.name));
            if (role)
                return {
                    id: role.id,
                    type: "ROLE",
                    permission: roleNamePermission.permission
                } as ApplicationCommandPermissionData;
            //TODO: \/ ?????? \/
            return {
                id: "0000000000",
                type: "ROLE",
                permission: roleNamePermission.permission
            } as ApplicationCommandPermissionData;
        })

        return this.userPermissions.concat(rolePermissions);
    }
}

export class SubCommandGrp {
    readonly subCommands: Array<SubCommand>;

    readonly name: string;
    readonly description: string;

    constructor(name: string, description: string, subCommands: Array<SubCommand> = []) {
        this.name = name;
        this.description = description;
        this.subCommands = subCommands;
    }

    public getCommandDefinition(): ApplicationCommandOptionData {
        return {
            "name": this.name,
            "description": this.description,
            "type" : "SUB_COMMAND_GROUP",
            "options": this.subCommands.map((subCommand: SubCommand) => {return subCommand.getCommandDefinition() as ApplicationCommandSubCommandData})
        };
    };
}

export class SubCommand {
    readonly name: string;
    readonly description: string;
    readonly options: Array<ApplicationCommandChoicesData | ApplicationCommandNonOptionsData>;
    public run: (commandInteraction: CommandInteraction) => void;

    constructor(name: string, description: string, options: Array<ApplicationCommandChoicesData | ApplicationCommandNonOptionsData> = [],
                run: (commandInteraction: CommandInteraction) => void) {
        this.name = name;
        this.description = description;
        this.options = options;
        this.run = run;
    }

    public getCommandDefinition(): ApplicationCommandSubCommandData {
        return {
            "name": this.name,
            "description": this.description,
            "type" : "SUB_COMMAND",
            "options": this.options,
        };
    };
}

export class ContextMenuCommand extends CommandGrp{

    public run: (contextMenuInteraction: ContextMenuInteraction) => void;

    constructor(name: string, type: "USER" | "MESSAGE",
                run: (contextMenuInteraction: ContextMenuInteraction) => void,
                roleNamePermissions: Array<{ name: String, permission: boolean }> = [],
                userPermissions: Array<ApplicationCommandPermissionData> = []) {
        super(name, "", [], [], roleNamePermissions, userPermissions, type);
        this.run = run;
    }

}