import {ContextMenuCommand, SubCommand} from "../../../../Command";
import {ApplicationCommandOptionData, CommandInteraction, GuildMember} from "discord.js";
import {log, logLevel} from "../../../../utils/log";
import {getRole} from "../../db/roles";

const userOpt: ApplicationCommandOptionData = {
    "name": "user",
    "description": "The user to Promote",
    "type": "USER",
    "required": true
}

export const PTGuest = new SubCommand(
    "guest",
    "Promote to Guest",
    [userOpt],
    commandInteraction => {
        Promote_to(commandInteraction, "guest");
    }
);

export const PTGuestContex = new ContextMenuCommand(
    "Promote to Guest",
    "USER",
    commandInteraction => {
        Promote_to(commandInteraction, "guest");
    },
    [
        {
            name: "@everyone",
            permission: false
        },
        {
            name: "member",
            permission: true
        }
    ]
);

export const PTMember = new SubCommand(
    "pt_member",
    "Promote to Member",
    [userOpt],
    commandInteraction => {
        Promote_to(commandInteraction, "member");
    }
);

export const PTMod = new SubCommand(
    "pt_mod",
    "Promote to Mod",
    [userOpt],
    commandInteraction => {
        Promote_to(commandInteraction, "mod");
    }
);

async function Promote_to(commandInteraction: CommandInteraction, role: string) {
    const guild = commandInteraction.guild;
    if (!guild) return;

    const tmpR = await getRole(guild.id, "tmp");
    const guestR = await getRole(guild.id, "guest");
    const memberR = await getRole(guild.id, "member");
    const modR = await getRole(guild.id, "mod");

    const user = commandInteraction.options.getUser("user", true);
    const member = await guild.members.fetch(user);

    await member.roles.add(guestR);
    await member.roles.remove(tmpR);
    if (role == "member" || role == "mod")  await member.roles.add(memberR);
    if (role == "mod")  await member.roles.add(modR);

    await commandInteraction.reply({content: member.displayName + " promoted to " + role, ephemeral: true});

    const emitter = commandInteraction.member as GuildMember;
    log(logLevel.WARN, `${emitter.displayName} promoted ${member.displayName} to ${role}`, guild.name);
}