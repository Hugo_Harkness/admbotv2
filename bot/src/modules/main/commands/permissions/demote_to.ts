import {SubCommand} from "../../../../Command";
import {ApplicationCommandOptionData, CommandInteraction, GuildMember} from "discord.js";
import {log, logLevel} from "../../../../utils/log";
import {getRole} from "../../db/roles";

const userOpt: ApplicationCommandOptionData = {
    "name": "user",
    "description": "The user to Demote",
    "type": "USER",
    "required": true
}

export const DTGuest = new SubCommand(
    "dt_guest",
    "Demote to Guest",
    [userOpt],
    commandInteraction => {
        Demote_to(commandInteraction, "guest");
    }
);

export const DTMember = new SubCommand(
    "dt_member",
    "Demote to Member",
    [userOpt],
    commandInteraction => {
        Demote_to(commandInteraction, "member");
    }
);

async function Demote_to(commandInteraction: CommandInteraction, role: string) {
    const guild = commandInteraction.guild;
    const emitter = commandInteraction.member as GuildMember;
    if (!guild) return;

    const memberR = await getRole(guild.id, "member");
    const modR = await getRole(guild.id, "mod");

    const user = commandInteraction.options.getUser("user", true);
    const member = await guild.members.fetch(user);

    if (role == "member" && member.roles.cache.has(modR.id))
        await member.roles.remove(modR);
    else if (role == "guest" && member.roles.cache.has(memberR.id)) {
        if (member.roles.cache.has(modR.id)) {
            await commandInteraction.reply({content: `${member.displayName} is a "mod" and can't be demoted to guest.`, ephemeral: true});
            log(logLevel.ERROR, `${emitter.displayName} tried to demoted ${member.displayName} (mod) to guest`, guild.name);
            return;
        }
        await member.roles.remove(memberR);
    }

    await commandInteraction.reply({content: member.displayName + " demoted to " + role, ephemeral: true});
    log(logLevel.WARN, `${emitter.displayName} demoted ${member.displayName} to ${role}`, guild.name);
}