import {SubCommand} from "../../../../Command";
import {log, logLevel} from "../../../../utils/log";
import {Message} from "discord.js";
import {getVoiceConnection} from "@discordjs/voice";

export const Ping = new SubCommand(
    "ping",
    "give WS heartbeat & Roundtrip latency.",
    [],
    async commandInteraction => {
        const sent = await commandInteraction.reply({ content: 'Pinging...', fetchReply: true }) as Message;
        const resultWS = `Websocket heartbeat: ${commandInteraction.client.ws.ping}ms.`;
        const resultRT = `Full API roundtrip latency: ${sent.createdTimestamp - commandInteraction.createdTimestamp}ms`;

        log(logLevel.TRACE, resultWS, "Ping");
        log(logLevel.TRACE, resultRT, "Ping");


        let resultVC = '';
        const voiceConnection = getVoiceConnection(commandInteraction.guild?.id as string);
        if ( voiceConnection != undefined) {
                resultVC = `Voice Connection ping: ws: ${voiceConnection.ping.ws} ms; udp: ${voiceConnection.ping.udp} ms`;
                log(logLevel.TRACE, resultVC, "Ping");
        }

        commandInteraction.editReply(`${resultWS}\n${resultRT}\n${resultVC}`);
    }
);