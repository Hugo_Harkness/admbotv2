import {SubCommand} from "../../../../Command";
import {GuildMember} from "discord.js";
import {log, logLevel} from "../../../../utils/log";
import {getRole} from "../../db/roles";

export const Masterkey = new SubCommand(
    "master_key",
    "Open all the doors",
    [],
    async commandInteraction => {
        const guild = commandInteraction.guild;
        const emitter = commandInteraction.member as GuildMember;
        if (!guild) return;

        const masterkeyR = await getRole(guild.id, "master_key");

        if (emitter.roles.cache.has(masterkeyR.id))
            await emitter.roles.remove(masterkeyR);
        else
            await emitter.roles.add(masterkeyR);


        await commandInteraction.reply({content: "success", ephemeral: true});
        log(logLevel.TRACE, `${emitter.displayName} used master_key`, guild.name);
    }
);