import {SubCommand} from "../../../../Command";
import {GuildMember} from "discord.js";
import {log, logLevel} from "../../../../utils/log";
import {getRole} from "../../db/roles";

export const Adm = new SubCommand(
    "adm",
    "Open all the doors (more)",
    [],
    async commandInteraction => {
        const guild = commandInteraction.guild;
        const emitter = commandInteraction.member as GuildMember;
        if (!guild) return;

        const admR = await getRole(guild.id, "adm");

        if (emitter.roles.cache.has(admR.id))
            await emitter.roles.remove(admR);
        else
            await emitter.roles.add(admR);


        await commandInteraction.reply({content: "success", ephemeral: true});
        log(logLevel.TRACE, `${emitter.displayName} used adm`, guild.name);
    }
);