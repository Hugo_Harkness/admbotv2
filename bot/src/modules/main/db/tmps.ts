import {dbs} from "../../../utils/db";
import {GuildMember} from "discord.js";

export interface tmpDoc {
    memberID: string,
    memberName: string,
    expTime: number,
    _id: string
}

export function tmpsDbInsert (guildMember: GuildMember) {
    dbs.get(guildMember.guild.id)?.tmps.insert({
        memberID: guildMember.id,
        memberName: guildMember.user.tag,
        expTime: new Date().getTime() + 12 * 60  * 60 * 1000,
    });
}

export function tmpsDbGetExps (guildId: string): Promise<Array<tmpDoc>> {
    return new Promise<Array<tmpDoc>>( (resolve, reject) => {
        dbs.get(guildId)?.tmps.find({"expTime": { $lte: new Date().getTime() }}, async (err: any, expMembersData: [tmpDoc]) => {
            if (err) reject(err);
            resolve(expMembersData);
        });
    });
}

export function tmpsDbRemove (guildId: string, _id: string) {
    dbs.get(guildId)?.tmps.remove({ _id: _id});
}