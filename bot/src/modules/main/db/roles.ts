import {ColorResolvable, Guild, PermissionResolvable, Role} from "discord.js";
import {log, logLevel, logTty} from "../../../utils/log";
import {dbs} from "../../../utils/db";

export async function initRolesDb(guild: Guild) {

    //get array
    let roles = guild.roles.cache.toJSON()

    //check all mandatory exist
    const mandatoryRoles: Array<string> = ["tmp", "guest", "member", "mod", "master_key", "adm"];
    let missingRoles: Array<string> = [];

    for (const mandatoryRole of mandatoryRoles){
        const role = roles.find(r => {return r.name == mandatoryRole})
        if (!role) missingRoles.push(mandatoryRole);
    }

    if (missingRoles.length > 0)
        logTty(logLevel.WARN, `Missing Roles : ${missingRoles.toString()}`, "Integrity");

    for (const missingRole of missingRoles){
        const newRole = await guild.roles.create({
            name: missingRole,
            hoist: getRoleSettings(missingRole).hoist,
            color: getRoleSettings(missingRole).color,
            permissions: getRoleSettings(missingRole).permissions,
            reason: "Base role."
        });
        roles.push(newRole);
    }

    //check integrity
    for (const role of roles){
        const roleSettings = getRoleSettings(role.name);
        const conform =
            role.color == roleSettings.color &&
            role.hoist == roleSettings.hoist &&
            role.permissions.equals(roleSettings.permissions);

        if (!conform && role.name != "Server Booster") {
            logTty(logLevel.WARN, `${role.name} non conform ...`, "Integrity");
            await role.setColor(roleSettings.color);
            await role.setHoist(roleSettings.hoist);
            await role.setPermissions(roleSettings.permissions);
        }
    }

    //sort
    roles.sort((ra, rb) => {
        return getRoleSettings(rb.name).pos - getRoleSettings(ra.name).pos;
    })

    let inOrder = true;
    for (let i = 0; i < roles.length - 1; i++){
        if (roles[i].comparePositionTo(roles[i+1]) < 0) inOrder = false;
    }

    //setpos
    if (!inOrder) {
        logTty(logLevel.WARN, `Sorting Roles ...`, "Integrity");
        for (const role of roles) {
            switch (role.name) {
                case "@everyone":
                    await role.setPosition(0);
                    break;
                case "tmp":
                    await role.setPosition(2);
                    break;
                case "guest":
                    await role.setPosition(3);
                    break;
                case "member":
                    await role.setPosition(4);
                    break;
                case "mod":
                    await role.setPosition(5);
                    break;
                case "master_key":
                    await role.setPosition(6);
                    break;
                case "adm":
                    await role.setPosition(7);
                    break;
                default:
                    await role.setPosition(1);
                    break;
            }
        }
    }

    //save in db
    for (const role of roles) {
        dbs.get(guild.id)?.roles.set(role.name, role);
    }

    logTty(logLevel.GOOD, `initRolesDb of ${guild.name}`, "Integrity")
}

export function getRole(guildID: string, name: string) {
    return new Promise<Role>((resolve, reject) => {
        const role = dbs.get(guildID)?.roles.get(name);
        if (!role) {
            log(logLevel.ERROR, `role "${name}" does not exist in "${guildID}" !`, "getRole()");
            reject();
            return;
        }
        resolve(role);
    });
}

function getRoleSettings(name: string) : {pos: number, hoist: boolean, color: ColorResolvable, permissions: PermissionResolvable} {
    switch (name) {
        case "@everyone":
            return {
                pos: 0,
                hoist: false,
                color: 0,
                permissions: ["USE_APPLICATION_COMMANDS"]
            };
        case "tmp":
            return {
                pos: 2,
                hoist: true,
                color: 0,
                permissions: ["ADD_REACTIONS", "STREAM", "VIEW_CHANNEL", "SEND_MESSAGES", "EMBED_LINKS", "ATTACH_FILES", "USE_EXTERNAL_EMOJIS", "CONNECT", "SPEAK", "USE_VAD", "CHANGE_NICKNAME"]
            };
        case "guest":
            return {
                pos: 3,
                hoist: true,
                color: 16777215,
                permissions: ["ADD_REACTIONS", "STREAM", "VIEW_CHANNEL", "SEND_MESSAGES", "EMBED_LINKS", "ATTACH_FILES", "USE_EXTERNAL_EMOJIS", "CONNECT", "SPEAK", "USE_VAD", "CHANGE_NICKNAME",
                    "CREATE_INSTANT_INVITE", "MANAGE_EMOJIS_AND_STICKERS", "READ_MESSAGE_HISTORY"]
            };
        case "member":
            return {
                pos: 4,
                hoist: true,
                color: 3447003,
                permissions: ["MENTION_EVERYONE"]
            };
        case "mod":
            return {
                pos: 5,
                hoist: true,
                color: 15844367,
                permissions: ["MOVE_MEMBERS", "MANAGE_MESSAGES", "VIEW_AUDIT_LOG"]
            };
        case "master_key":
            return {
                pos: 6,
                hoist: true,
                color: 15158332,
                permissions: []
            };
        case "adm":
            return {
                pos: 7,
                hoist: true,
                color: 15158332,
                permissions: ["ADMINISTRATOR"]
            }
        default:
            return {
                pos: 1,
                hoist: false,
                color: 0,
                permissions: [],
                //mentionable: true,
            };
    }
}