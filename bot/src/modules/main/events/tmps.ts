import {Client, GuildMember} from "discord.js";
import {log, logLevel} from "../../../utils/log";
import {tmpsDbGetExps, tmpsDbInsert, tmpsDbRemove} from "../db/tmps";
import {getRole} from "../db/roles";

export async function newTmps(guildMember: GuildMember) {

    const tmp = await getRole(guildMember.guild.id, "tmp");

    guildMember.roles.add(tmp).then();
    tmpsDbInsert(guildMember);

    log(logLevel.GOOD, `${guildMember.user?.tag} joined.`, guildMember.guild.name);

}

export function setIntervalClearTmps(client: Client) {
    setInterval(() => {
        client.guilds.cache.each(async (guild) => {

            const tmp = await getRole(guild.id, "tmp");

            const expMembersData = await tmpsDbGetExps(guild.id);

            for (const expMemberData of expMembersData) {
                try {
                    const expMember = await guild.members.fetch(expMemberData.memberID);

                    if (expMember.roles.cache.has(tmp.id)) {
                        await expMember.kick("12 hour delay expired.");
                        log(logLevel.ERROR, `${expMember.user?.tag} kicked : 12 hour delay expired.`, guild.name);
                    }
                }catch (error) {
                    log(logLevel.ERROR, `${expMemberData.memberID} removed from tmps.db : 12 hour delay expired.`, guild.name);
                }finally {
                    tmpsDbRemove(guild.id, expMemberData._id);
                }

            }

        });
    }, 6 * 60 * 60 * 1000);
}