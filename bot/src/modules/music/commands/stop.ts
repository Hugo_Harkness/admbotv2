import {CommandInteraction} from "discord.js";
import {getMusicDB} from "../db/musicDB";
import {SubCommand} from "../../../Command";

async function stop(commandInteraction: CommandInteraction) {

    const musicDB = await getMusicDB(commandInteraction.guild?.id as string);
    musicDB.queue.splice(0,musicDB.queue.length);
    musicDB.player.stop();

    commandInteraction.reply({content: "ok.", ephemeral: true});
}

export const Stop = new SubCommand(
    "stop",
    "stop music & clear queue",
    [],
    stop,
);