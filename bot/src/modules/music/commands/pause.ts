import {SubCommand} from "../../../Command";
import {CommandInteraction} from "discord.js";
import {getMusicDB} from "../db/musicDB";

async function pause(commandInteraction: CommandInteraction) {

    const musicDB = await getMusicDB(commandInteraction.guild?.id as string);
    if (musicDB.player.state.status == "playing") musicDB.player.pause(true);
    else if (musicDB.player.state.status == "paused") musicDB.player.unpause();

    commandInteraction.reply({content: "ok.", ephemeral: true});
}

export const Pause = new SubCommand(
    "pause",
    "pause or unpause music",
    [],
    pause,
);