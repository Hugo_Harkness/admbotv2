import {CommandInteraction, MessageEmbed} from "discord.js";
import {getMusicDB} from "../db/musicDB";
import {SubCommand} from "../../../Command";

async function queue(commandInteraction: CommandInteraction) {

    const musicDB = await getMusicDB(commandInteraction.guild?.id as string);

    let embed = new MessageEmbed()
    embed.setTitle(`Playing:`);
    embed.setDescription(`${musicDB.songNow?.title}`);
    if(musicDB.queue && musicDB.queue.length > 0) {
        let next = "";
        musicDB.queue.forEach(song => {
            next += `${song.title}\n`
        })
        embed.addField("Queue:", next);
    }

    commandInteraction.reply({embeds: [embed], ephemeral: true});
}

export const Queue = new SubCommand(
    "queue",
    "show music queue",
    [],
    queue,
);