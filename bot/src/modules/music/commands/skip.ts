import {CommandInteraction} from "discord.js";
import {getMusicDB} from "../db/musicDB";
import {SubCommand} from "../../../Command";

async function skip(commandInteraction: CommandInteraction) {

    const musicDB = await getMusicDB(commandInteraction.guild?.id as string);
    musicDB.player.stop();

    commandInteraction.reply({content: "ok.", ephemeral: true});
}

export const Skip = new SubCommand(
    "skip",
    "skip music",
    [],
    skip,
);