import {getMusicDB, MusicDB, Song} from "../db/musicDB";
import {
    createAudioResource, demuxProbe, entersState,
    getVoiceConnection,
    joinVoiceChannel,
    VoiceConnectionStatus
} from "@discordjs/voice";
import {SubCommand} from "../../../Command";
import {ApplicationCommandOptionData, CommandInteraction, GuildMember} from "discord.js";
import ytdl from 'ytdl-core';
import {logTty} from "../../../utils/log";

export async function playNext(guildID: string) {
    const musicDB = await getMusicDB(guildID);
    const song = musicDB.queue.shift();
    musicDB.songNow = song;
    if(!song){
        getVoiceConnection(guildID)?.disconnect();
        musicDB.songNow = undefined;
        return;
    }
    const readableStream = ytdl(song.url, { filter: 'audioonly' , highWaterMark: 1024 * 1024 * 10});
    const { stream, type } = await demuxProbe(readableStream);
    const resource = createAudioResource(stream, { inputType: type/*, inlineVolume: true*/});
    //resource.volume?.setVolume(0.25);
    musicDB.player.play(resource);
}

async function makeSong(url: string): Promise<Song | undefined> {
    return new Promise<Song | undefined>((resolve, rejects) => {
        ytdl.getInfo(url)
            .then(async value => {
                resolve({
                    url: url,
                    title: value.videoDetails.title,
                });
            })
            .catch(reason => {
                resolve(undefined);
            });
    });
}

async function addToQueue(guildID: string, song: Song) {
    const musicDB = await getMusicDB(guildID);
    musicDB.queue.push(song);

    if (musicDB.player.state.status == "paused")
        musicDB.player.unpause();
    else if ( musicDB.player.state.status == 'idle')
        playNext(guildID);
}

async function play(commandInteraction: CommandInteraction) {
    const member = commandInteraction.member as GuildMember;
    const guild = commandInteraction.guild;
    const voiceChannel = member.voice.channel;

    if (!voiceChannel || !guild) {
        //TODO: ERROR !
        commandInteraction.reply({content: "you need to be in an voice channel.", ephemeral: true})
        return;
    }

    const song = await makeSong(commandInteraction.options.getString("yt_url", true));
    if (!song) {
        //TODO: ERROR !
        commandInteraction.reply({content: "invalid url", ephemeral: true})
        return;
    }


    //TODO: TRACE !
    commandInteraction.reply({content: `"${song.title}" added to queue`, ephemeral: true})
    await addToQueue(guild.id, song);

    if (getVoiceConnection(guild.id) == undefined) {
        const voiceConnection = joinVoiceChannel({
            channelId: voiceChannel.id,
            guildId: guild.id,
            adapterCreator: guild.voiceAdapterCreator,
            selfDeaf: true,
            selfMute: false,
        });
        voiceConnection.on(VoiceConnectionStatus.Ready, async () => {
            voiceConnection.subscribe((await getMusicDB(guild.id)).player);
        });
        voiceConnection.on(VoiceConnectionStatus.Disconnected, async (oldState, newState) => {
            try {
                await Promise.race([
                    entersState(voiceConnection, VoiceConnectionStatus.Signalling, 5_000),
                    entersState(voiceConnection, VoiceConnectionStatus.Connecting, 5_000),
                ]);
                // Seems to be reconnecting to a new channel - ignore disconnect
            } catch (error) {
                // Seems to be a real disconnect which SHOULDN'T be recovered from
                voiceConnection.destroy();
            }
        });
    }
}

const urlOpt: ApplicationCommandOptionData = {
    "name": "yt_url",
    "description": "Youtube URL",
    "type": "STRING",
    "required": true
}

export const Play = new SubCommand(
    "play",
    "Play",
    [urlOpt],
    play,
);



