import {CommandInteraction, GuildMember} from "discord.js";
import {entersState, getVoiceConnection, joinVoiceChannel, VoiceConnectionStatus} from "@discordjs/voice";
import {getMusicDB} from "../db/musicDB";
import {SubCommand} from "../../../Command";

async function move(commandInteraction: CommandInteraction) {
    const member = commandInteraction.member as GuildMember;
    const guild = commandInteraction.guild;
    const voiceChannel = member.voice.channel;

    if (!voiceChannel || !guild) {
        //TODO: ERROR !
        commandInteraction.reply({content: "you need to be in an voice channel.", ephemeral: true})
        return;
    }

    getVoiceConnection(guild.id)?.disconnect();

        const voiceConnection = joinVoiceChannel({
            channelId: voiceChannel.id,
            guildId: guild.id,
            adapterCreator: guild.voiceAdapterCreator,
            selfDeaf: true,
            selfMute: false,
        });
        voiceConnection.on(VoiceConnectionStatus.Ready, async () => {
            voiceConnection.subscribe((await getMusicDB(guild.id)).player);
        });
        voiceConnection.on(VoiceConnectionStatus.Disconnected, async (oldState, newState) => {
            try {
                await Promise.race([
                    entersState(voiceConnection, VoiceConnectionStatus.Signalling, 5_000),
                    entersState(voiceConnection, VoiceConnectionStatus.Connecting, 5_000),
                ]);
                // Seems to be reconnecting to a new channel - ignore disconnect
            } catch (error) {
                // Seems to be a real disconnect which SHOULDN'T be recovered from
                voiceConnection.destroy();
            }
        });

    commandInteraction.reply({content: "ok", ephemeral: true})
}

export const Move = new SubCommand(
    "move",
    "move music bot to your channel",
    [],
    move,
);