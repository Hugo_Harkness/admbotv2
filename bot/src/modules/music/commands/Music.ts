import {CommandGrp} from "../../../Command";
import {Play} from "./play";
import {Pause} from "./pause";
import {Skip} from "./skip";
import {Stop} from "./stop";
import {Move} from "./move";
import {Queue} from "./queue";

const everyone = {
    name: "@everyone",
    permission: false
}

const guest = {
    name: "guest",
    permission: true
}

export const Music = new CommandGrp(
    "music",
    "Music",
    [],
    [Play, Pause, Skip, Stop, Move, Queue],
    [guest,everyone]
);