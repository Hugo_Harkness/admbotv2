import {dbs} from "../../../utils/db";
import {AudioPlayer, AudioPlayerStatus, AudioResource, createAudioPlayer} from "@discordjs/voice";
import {playNext} from "../commands/play";
import {logLevel, logTty} from "../../../utils/log";

export interface MusicDB {
    queue: Array<Song>,
    songNow: Song | undefined,
    player: AudioPlayer,
}

export interface Song {
    url: string
    title: string
}

export function initPlayer(guildID: string) {
    const player = createAudioPlayer();
    player.on(AudioPlayerStatus.Idle, () => playNext(guildID));
    player.on("error", (error) => {
        logTty(logLevel.ERROR, `${error.message}`, 'ap error')
    })
    return player;
}

export function getMusicDB(guildID: string) {
    return new Promise<MusicDB>((resolve, rejects) => {
        const musics = dbs.get(guildID)?.musics;
        if(musics) resolve(musics);
        else rejects();
    })
}