import {Client, Intents} from 'discord.js';
import {readFileSync} from "fs";
import {ready} from "./events/ready";
import {interactionCreate} from "./events/interactionCreate";
import {guildMemberAdd} from "./events/guildMemberAdd";
import {log, logLevel, logTty} from "./utils/log";

const client = new Client({ intents: [
            Intents.FLAGS.GUILDS,
            Intents.FLAGS.GUILD_MESSAGES,
            Intents.FLAGS.GUILD_MEMBERS,
            Intents.FLAGS.GUILD_PRESENCES,
            Intents.FLAGS.GUILD_VOICE_STATES
    ] });

//Events :
client.on("ready", ready);
client.on('guildMemberAdd', guildMemberAdd);
client.on("interactionCreate", interactionCreate);

client.on("error", args => logTty(logLevel.ERROR, `${args}`, "ERROR") );
client.on("warn", args => logTty(logLevel.WARN, `${args}`, "WARN") );
client.on("rateLimit", info => logTty(logLevel.ERROR, `${info.limit} : ${info.timeout} : ${info.route}`, "rateLimit") );

let token = readFileSync("data/token").toString().trim();
client.login(token).then(() => {log(logLevel.WARN, "Connecting ...", "Login")});