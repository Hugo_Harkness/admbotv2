import {GuildMember} from "discord.js";
import {newTmps} from "../modules/main/events/tmps";

export function guildMemberAdd(guildMember: GuildMember) {
    newTmps(guildMember);
}