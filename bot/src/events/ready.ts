import {commands} from "../CommandList";
import {Client} from "discord.js";
import {logLevel, logTty} from "../utils/log";
import {dbInit} from "../utils/db";
import {setIntervalClearTmps} from "../modules/main/events/tmps";

export function ready(client: Client) {
    //Bot Status
    client.user?.setActivity('V2: WIP', { type: 'WATCHING' });

    //Log Login message
    logTty(logLevel.TRACE, `Logged in as "${client.user?.tag}" with id: ${client.user?.id} on:`, "Ready");
    client.guilds.cache.each(guild => {
        logTty(logLevel.TRACE, `-- "${guild.name}" id: ${guild.id}`, "Ready");
    });

    //Load Databases
    client.guilds.cache.each(dbInit);

    //Register commands
    registerCommands(client);

    //setIntervals
    setIntervalClearTmps(client);

    //TODO: wait for all to be ready
}

function registerCommands(client: Client) {
    client.guilds.cache.each(async (guild) => {

        let guildCommands = await guild.commands.set(commands.map((command) => {
            return command.getCommandDefinition()
        }));

        guildCommands.forEach((guildCommand) => {
            commands.forEach(command => {
                if (command.name == guildCommand.name)
                    guild.commands.permissions.set({command:guildCommand.id, permissions:command.getPermissions(guild)});
            })
        })

    });
}