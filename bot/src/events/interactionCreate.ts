import {
    CommandInteraction,
    ContextMenuInteraction,
    Interaction,
} from "discord.js";
import {commands} from "../CommandList";

export function interactionCreate(interaction: Interaction) {
    if(interaction.isCommand()) {
        let CInteraction = interaction as CommandInteraction;
        commands.filter(command => (CInteraction.commandName == command.getCommandDefinition().name))[0].run(CInteraction);
    } else if(interaction.isContextMenu()) {
        let CMInteraction = interaction as ContextMenuInteraction;
        commands.filter(command => (CMInteraction.commandName == command.getCommandDefinition().name))[0].run(CMInteraction);
    }
}