import {default as Datastore} from '@yetzt/nedb';
import {Guild, Role} from "discord.js";
import {initRolesDb} from "../modules/main/db/roles";
import { initPlayer, MusicDB, Song} from "../modules/music/db/musicDB";

export interface guildDb {
    tmps: Datastore
    roles: Map<string, Role>
    musics: MusicDB
}

export const dbs: Map<string,guildDb> = new Map<string, guildDb>();

export async function dbInit(guild: Guild) {
    dbs.set(guild.id, {
        tmps: new Datastore(`data/db/${guild.id}/tmps.db`),
        roles: new Map<string, Role>(),
        musics: {
            queue: new Array(),
            songNow: undefined,
            player: initPlayer(guild.id),
        },
    });

    dbs.get(guild.id)?.tmps.loadDatabase();
    await initRolesDb(guild);
}