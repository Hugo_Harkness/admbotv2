import {CommandGrp} from "./Command";
import {ApplicationCommandPermissionData} from "discord.js";
import {PTGuest, PTGuestContex, PTMember, PTMod} from "./modules/main/commands/permissions/promote_to";
import {DTGuest, DTMember} from "./modules/main/commands/permissions/demote_to";
import {Adm} from "./modules/main/commands/utils/adm";
import {Masterkey} from "./modules/main/commands/utils/masterkey";
import {Ping} from "./modules/main/commands/utils/ping";
import {Music} from "./modules/music/commands/Music";

const everyone = {
    name: "@everyone",
    permission: false
}

const ownerPerm: ApplicationCommandPermissionData = {
    id: "389432812266192906",
    type: "USER",
    permission: true
}

const mod = {
    name: "mod",
    permission: true
}

const member = {
    name: "member",
    permission: true
}

/*
const guest = {
    name: "guest",
    permission: true
}

const tmp = {
    name: "tmp",
    permission: true
}
*/

const Ptg = new CommandGrp(
    "promote_to",
    "Promote to guest",
    [],
    [PTGuest],
    [member,everyone],
);

const Owner = new CommandGrp(
    "owner",
    "owner cmds",
    [],
    [Adm, PTMod, DTMember],
    [everyone],
    [ownerPerm]
);

const Mod = new CommandGrp(
    "mod",
    "mod cmds",
    [],
    [Masterkey, Ping, PTMember, DTGuest],
    [mod,everyone]
);

/*
const List = new CommandGrp(
    "list",
    "show list of ...",
    [],
    [],
    [guest,everyone]
);

const Make = new CommandGrp(
    "make",
    "make ...",
    [],
    [],
    [member,everyone]
);

const Delete = new CommandGrp(
    "delete",
    "delete ...",
    [],
    [],
    [member,everyone]
);

const Edit = new CommandGrp(
    "edit",
    "edit ...",
    [],
    [],
    [member,everyone]
);

const Join = new CommandGrp(
    "join",
    "join ...",
    [],
    [],
    [guest,everyone]
);

const Leave = new CommandGrp(
    "leave",
    "leave ...",
    [],
    [],
    [guest,everyone]
);
*/

//-----------------------------------------------------

export let commands: CommandGrp[] = [
    Ptg, Owner, Mod,// List, Make, Delete, Edit, Join, Leave,
    Music,
    PTGuestContex,
];